#! -*- coding: utf-8 -*-
import Bio.PDB
from Bio.PDB.Vector import *
from modeller import *
from modeller.automodel import *
## Set all Modeller log output levels
log.level(output=0, notes=0, warnings=0, errors=0, memory=0)
import numpy
from pylab import *
import os
import shutil
import math
from geometrie import *

   
    
def distance_droite_point( point_A, point_B, point_M ):
    """
    Calcul distance between point M and
    a droite passing by the point A and the point B
    H is the projected of M on AB
    H=A+k*(A-B)
    """
    
    xa,ya,za=point_A
    xb,yb,zb=point_B
    xm,ym,zm=point_M
    
    xab=xa-xb
    yab=ya-yb
    zab=za-zb
    
    xma=xm-xa
    yma=ym-ya
    zma=zm-za
    
    if (xab * xma + yab * yma + zab * zma)!=0:
        k=(xab * xma + yab * yma + zab * zma)/float((xab*xab + yab*yab + zab*zab))
        
        xh = xa + k * (xa-xb)
        yh = ya + k * (ya-yb)
        zh = za + k * (za-zb)
        
        point_H = [xh, yh, zh]
        
        return distance(point_H, point_M)
    else:
        return distance(point_A, point_M)



def stat_moyenne( echantillon ) :
    taille = len( echantillon )
    moyenne = sum( echantillon ) / taille
    return moyenne

def stat_variance( echantillon ) :
    n = len( echantillon ) # taille
    mq = stat_moyenne( echantillon )**2
    s = sum( [ x**2 for x in echantillon ] )
    variance = s / n - mq
    return variance

def stat_ecart_type( echantillon ) :
    variance = stat_variance( echantillon )
    ecart_type = math.sqrt( variance )
    return ecart_type



def RMSD_calc(tab_atoms, reference=None):
    n_atoms=len(tab_atoms)
    if reference == None:
        barycenter=0.0,0.0,0.0
        for i in xrange(1, n_atoms):
            barycenter+=tab_atoms[i].get_coord()
        for i in [0,1,2]:
            barycenter[i]=barycenter[i]/n_atoms
    else:
        barycenter=reference.get_coord()
    
    
    total=0
    for i in xrange(n_atoms):
        total+=(distance(tab_atoms[i].get_coord(), barycenter))**2
    
    return sqrt(total/n_atoms)

def distance(coord1, coord2):
    return sqrt((coord1[0]-coord2[0])**2+
                (coord1[1]-coord2[1])**2+
                (coord1[2]-coord2[2])**2)
    
def calcul_angle(point1, point2, point3):
    
    x1,y1,z1=point1
    x2,y2,z2=point2
    x3,y3,z3=point3
        
    vec1=[x1-x2, y1-y2, z1-z2]
    vec2=[x3-x2, y3-y2, z3-z2]

    return calcul_angle_vector(vec1, vec2)

def calcul_angle_vector(vec1, vec2):
    
    try:
        div=(vec1[0]*vec2[0]+vec1[1]*vec2[1]+vec1[2]*vec2[2])/(distance(vec1,[0,0,0])*distance(vec2,[0,0,0]))
        if div>1:
            div=1
        if div<-1:
            div=-1
        angle=180/math.pi*math.acos(div)
    except:
        print vec1
        print vec2
        print (vec1[0]*vec2[0]+vec1[1]*vec2[1]+vec1[2]*vec2[2])/(distance(vec1,[0,0,0])*distance(vec2,[0,0,0]))
        
    return angle

####################################### A COMPLETER #############################################
def find_site_actif(modele):
    return [1,2,3,4,5,6,7,8,9]
def def_donn_acc(model):
    return [1,2,3,4,5,6,7,8,9],[1,2,3,4,5,6,7,8,9]
def def_ions(model):
    return [1,2,3,4,5,6,7,8,9]
def def_aromatics(model):
    return [1,2,3,4,5,6,7,8,9]



def translation(vecteur_coord, valeur, axe):

    size=sqrt(axe[0]*axe[0]+axe[1]*axe[1]+axe[2]*axe[2])
    u_vecteur=axe/size

    
    vecteur_coord[0]=vecteur_coord[0]-valeur*u_vecteur[0]
    vecteur_coord[1]=vecteur_coord[1]-valeur*u_vecteur[1]
    vecteur_coord[2]=vecteur_coord[2]-valeur*u_vecteur[2]
    return vecteur_coord
    
    
    
    
def rotation(vecteur_coord, axe, angle, centre=None):
    ## Rotation autour d'un axe
    ## http://fr.wikipedia.org/wiki/Matrice_de_rotation
    ## Transformation du vecteur en vecteur unitaire
    

    if centre is not None:
        vecteur_coord[0]=vecteur_coord[0]-centre[0]
        vecteur_coord[1]=vecteur_coord[1]-centre[1]
        vecteur_coord[2]=vecteur_coord[2]-centre[2]

    size=sqrt(axe[0]*axe[0]+axe[1]*axe[1]+axe[2]*axe[2])
    u_vecteur=axe/size
    ux=u_vecteur[0]
    uy=u_vecteur[1]
    uz=u_vecteur[2]
    
    
    ## Calculs
    ## R = P +(I-P)cos(angle) + Q*sin(angle)
    P=np.matrix([[ux*ux, ux*uy, ux*uz],
                 [ux*uy, uy*uy, uy*uz],
                 [ux*uz, uy*uz, uz*uz]])
    I=np.matrix([[1, 0, 0],
                 [0, 1, 0],
                 [0, 0, 1]])
    Q=np.matrix([[ 0, -uz,  uy],
                 [uz,   0, -ux],
                 [-uy, ux,  0]])
    
    
    R=P+(I-P)*cos(angle)+Q*sin(angle)
    
    vecteur_coord_modif=np.array(np.dot(R,vecteur_coord))[0]
    
    if centre is not None:
        vecteur_coord_modif[0]=vecteur_coord_modif[0]+centre[0]
        vecteur_coord_modif[1]=vecteur_coord_modif[1]+centre[1]
        vecteur_coord_modif[2]=vecteur_coord_modif[2]+centre[2]

    return vecteur_coord_modif

    
def determine_axe(selection, direction, _type="normal"):

    assert _type in ["normal","perp"], "ERROR"
    # Do an SVD on the mean-centered data.
    data = np.array([atom.get_coord() for atom in selection])
    datamean=data.mean(axis=0)
    if direction==None:
        uu, dd, vv = np.linalg.svd(data - datamean)
        axe1=vv[0]
        direction=axe1

        if _type=="normal":
            return direction
    
    x2=datamean[0]-direction[0]
    y2=datamean[1]-direction[1]
    z2=datamean[2]-direction[2]
    axe2=[x2,y2,z2]
        
    if _type=="perp":
        return axe2

    ## On determine les axes perpendiculaires
    x1=axe1[0]
    y1=axe1[1]
    z1=axe1[2]
    x3=1
    ## Calcul apres developpement des produits scalaires
    y3=(z1/z2*x2*x3-x1*x3)/(y1-z1/z2*y2)
    z3=-(x2*x3+y2*y3)/z2

    
    axe3=[x3,y3,z3]

    return axe3


############## clean_tempfile ##############

def clean_tempfile():
    os.system('rm *.ini *.sch *.rsr *.D000* *.V999* *.asa *.log *.rsa+ > /dev/null 2>&1')

############## move_files ##############
        
def move_files(dir_name):
    try:
        os.mkdir(dir_name)
    except:
        pass
    for filename in os.listdir('.'):
        if ".B99" in filename or '.png' in filename or 'colored' in filename or 'conservation' in filename:
            shutil.move(filename, dir_name)











############## get_sequences ##############    

def get_sequences(filename, sequence_name, pdb_template):
        
    filein=open(filename,'r')
    i=0
    lines=filein.readlines()
    while ">" not in lines[i]:
        i+=1
    name1=lines[i].split(';')[1].strip()
    i+=2
    seq1=""
    while "*" not in seq1:
        seq1+=lines[i].strip()
        i+=1
            
    while ">" not in lines[i]:
        i+=1
    name2=lines[i].split(';')[1].strip()
    i+=2
    seq2=""
    while "*" not in seq2:
        seq2+=lines[i].strip()
        i+=1
    filein.close()
        
    assert len(seq1)==len(seq2), "Sequence length differe between sequence and template"

    if (name1,name2)==(sequence_name,pdb_template):
        return seq2, seq1
    elif (name1,name2)==(pdb_template,sequence_name):
        return seq1,seq2
    else:
        print "ERROR: Sequences cannot be found"
        print name1,name2
        print pdb_template,sequence_name
        return '',''
            

############## clear_restraints ##############

def clear_restraints(self):
    restraints=[]

############## add_restraint ##############
    
def add_restraint(self, restraint):
    if isinstance(restraint, list):
        if restraint not in self.restraints:
            self.restraints.append(restraint)
        else:
            'WARNING: add_restraints argument already in restraints list\n         Use get_restraints to see restraints list'
    else:
            print 'ERROR: add_restraints argument have to be a list'

############## del_restraints ##############
        
def del_restraint(self, restraint):
    if isinstance(restraint, list):
        if restraint in self.restraints:
            self.restraints.append(restraint)
        else:
            'WARNING: del_restraints argument not in restraints list\n         Use get_restraints to see restraints list'
    else:
            print 'ERROR: del_restraints argument have to be a list'











